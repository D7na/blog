class ArticlesController < ApplicationController

def new
	@article = Article.new
end

def edit
	@article = Article.find(params[:id])
end

def update
	@article = Article.find(params[:id])

	if @article.update(params_article)
		redirect_to @article
	else
		render 'edit'
	end
end

def destroy
	@article = Article.find(params[:id])
	@article.destroy

	redirect_to articles_path
end

def create
	# render plain: params[:article].inspect
	@article = Article.new(params_article)
	
	if @article.save
		redirect_to @article
	else
		render 'new'
	end
end

def show
	@article = Article.find(params[:id])
	respond_to do |format|
   format.html
   format.pdf do
     render pdf: "diary_article",
     template: "articles/show.html.erb",
     layout: 'pdf.html',
     encoding: 'utf8'
   	end
   end
end

def index
	if params[:tag]
		@article = Article.tagged_with(params[:tag]).order(created_at: :desc).page(params[:page])
	else
		@article = Article.all.order(created_at: :desc).page params[:page]
	# @article.order(:date).page params[:page]
	end
end

private

def params_article
	params.require(:article).permit(:title, :text, :tag_list)
end
end