Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, controllers: { registrations: "registrations"}
  devise_scope :user do
    root to: "devise/sessions#new"
  end
  get 'tags/:tag', to: 'articles#index', as: :tag
  resources :articles do
    resources :comments
  end 
end
 
